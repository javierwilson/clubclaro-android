package club.claro;


import club.claro.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
 
public class MainActivity extends Activity {

	protected static final String TAG = null;
	private WebView mWeb;
	private ProgressDialog mProgress;  
 
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
 
        setContentView(R.layout.activity_main);
        
        mWeb = new WebView(this);
        setContentView(mWeb);
 
        WebSettings settings = mWeb.getSettings();
        settings.setJavaScriptEnabled(true);
        
        mProgress = ProgressDialog.show(this, "Club Claro", "Cargando, favor espere");
        
        mWeb.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
            	if (url.startsWith("tel:")) { 
            		Intent intent = new Intent(Intent.ACTION_DIAL,
            				Uri.parse(url)); 
                    startActivity(intent); 
            	} else if (url.startsWith("geo:")) {
                    Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse(url)); 
                    startActivity(searchAddress); 
            	} else if (url.startsWith("sms:")) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW);

                        // Get address
                        String address = null;
                        int parmIndex = url.indexOf('?');
                        if (parmIndex == -1) {
                            address = url.substring(4);
                        }
                        else {
                            address = url.substring(4, parmIndex);

                            // If body, then set sms body
                            Uri uri = Uri.parse(url);
                            String query = uri.getQuery();
                            if (query != null) {
                                if (query.startsWith("body=")) {
                                    intent.putExtra("sms_body", query.substring(5));
                                }
                            }
                        }
                        intent.setData(Uri.parse("sms:"+address));
                        intent.putExtra("address", address);
                        intent.setType("vnd.android-dir/mms-sms");
                        startActivity(intent);
                    } catch (android.content.ActivityNotFoundException e) {
                        Log.e(TAG, "Error sending sms "+url+":"+ e.toString());
                    }
            	}
            	else {
            		view.loadUrl(url);
            	}
            	return true;
         }
            public void onPageFinished(WebView view, String url)
            {
            	if (mProgress.isShowing())
            	{
            		mProgress.dismiss();
            	}
            }
        });
        
        
        mWeb.loadUrl("http://clubclaro.com.ni");
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode)
            {
            case KeyEvent.KEYCODE_BACK:
                if(mWeb.canGoBack() == true){
                    mWeb.goBack();
                }else{
                    finish();
                }
                return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.layout.menu, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        
        alertDialog.setTitle("Salir ...");
        alertDialog.setMessage("Esta seguro de cerrar la aplicacion?");
        alertDialog.setIcon(R.drawable.ic_menu_close);
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	finish();
            }
        });

        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.dismiss();
            }
        });

    	
    	switch (item.getItemId())
        {
        case R.id.close:
            alertDialog.show();
            break;
        default:
            return super.onOptionsItemSelected(item);
        }
		return false;
    }    



    

}